echo "* Adding hosts ..."
echo "192.168.111.100 ansible.host.dob.exam ansible" >> /etc/hosts
echo "192.168.111.101 jenkins.host.dob.exam jenkins" >> /etc/hosts
echo "192.168.111.102 nagios.host.dob.exam nagios" >> /etc/hosts
echo "192.168.111.103 docker.host.dob.exam docker" >> /etc/hosts

echo "* Clean cache, update and upgrade (Required for install ansible) ..."
sudo dnf clean all && rm -r /var/cache/dnf  && dnf upgrade -y && dnf update -y 

echo "* Installing Ansible ..."
sudo dnf install -y epel-release
sudo dnf install -y ansible

echo "* Set Ansible configuration in /etc/ansible/ansible.cfg ..."
sudo cp /vagrant/ansible.cfg /etc/ansible/ansible.cfg

echo "* Set Ansible global inventory in /etc/ansible/hosts ..."
sudo cp /vagrant/hosts /etc/ansible/hosts

echo "* Copy Ansible playbooks in /playbooks/ ..."
sudo cp -R /vagrant/playbooks /playbooks
 
echo "* Prepare roles folder for playbooks ..."
sudo cp -R /vagrant/roles /playbooks

echo "* Install Ansible roles for Docker and Jenkins ..."
sudo ansible-galaxy install geerlingguy.jenkins -p /playbooks/roles/
sudo ansible-galaxy install geerlingguy.docker -p /playbooks/roles/
sudo ansible-galaxy install geerlingguy.java -p /playbooks/roles/

echo "* Execute Ansible Playbooks ..."
sudo ansible-playbook /playbooks/install-all.yml
