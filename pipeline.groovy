pipeline {
   agent {
      label 'slave'
   }
   stages {
      stage('Git clone') {
        steps {
          echo 'Clone github repo...'
          git 'https://github.com/shekeriev/dob-2020-05-exam'
        }
      }
      stage('Docker build') {
       steps {
          echo 'Build docker image'
          sh 'docker image build -t mariadb/Dockerfile'
          sh 'docker image build -t nginx/Dockerfile'
          sh 'docker image build -t php/Dockerfile'
       }
     }
     stage('Docker run') {
       steps {
         echo 'Run docker container'
         sh 'docker container run -t mariadb/Dockerfile'
         sh 'docker container run -t nginx/Dockerfile'
         sh 'docker container run -t php/Dockerfile'
       }
     }
   }
}
