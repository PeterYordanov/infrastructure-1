echo "* Setting up ..."
sudo yum install -y epel-release
sudo yum config-manager --set-enabled PowerTools

echo "* Installing required packages ..."
sudo yum install -y nrpe nrpe-selinux nagios-plugins-nrpe nagios-plugins-all nagios nagios-common nagios-selinux

echo "* Opening port ..."
sudo firewall-cmd --add-port=5666/tcp --permanent
sudo firewall-cmd --reload
sudo systemctl enable --now nrpe
